const todoList = [
  {
    id: 1,
    text: '👋 Meet and Greet',
    completed: true,
  },
  {
    id: 2,
    text: 'Meeting at School',
    completed: false,
  },
  {
    id: 3,
    text: '🛠 Node JS',
    completed: false,
  },
  {
    id: 4,
    text: '🛠 Golang',
    completed: false,
  },
  {
    id: 5,
    text: '🔈 Node JS',
    completed: false,
  },
  {
    id: 6,
    text: '👨🏻‍💻 Current Work',
    completed: false,
  },
  {
    id: 7,
    text: '🛠 Learn Spring Boot',
    completed: false,
  },
  {
    id: 8,
    text: '🛠 Learn Docker',
    completed: false,
  },
  {
    id: 9,
    text: '🛠 Learn AWS',
    completed: false,
  },
];

export default todoList;
