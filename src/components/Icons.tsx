import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';

type IconsProps = React.PropsWithChildren<{
  name: string;
  color: string;
  size: number;
}>;

const Icons = ({name, color}: IconsProps) => {
  return <Icon name={name} size={28} color={color} />;
};

export default Icons;
