import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {GlobalStyles} from '../constants/style';
import ToDoList from '../screens/todo/todolist';
import Icons from './components/Icons';
import {StatusBar} from 'react-native';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const App = () => {
  const HomeScreen = React.useCallback(() => {
    return (
      <Tab.Navigator
        initialRouteName="All ToDos"
        screenOptions={{
          tabBarActiveTintColor: GlobalStyles.colors.primary50,
          tabBarActiveBackgroundColor: GlobalStyles.colors.primary50,
        }}>
        <Tab.Screen
          name="AllToDos"
          component={ToDoList}
          options={{
            title: 'All ToDos',
            tabBarLabel: 'All ToDos',
            tabBarActiveTintColor: GlobalStyles.colors.primary400,
            // eslint-disable-next-line react/no-unstable-nested-components
            tabBarIcon: ({size}) => (
              <Icons
                name="home"
                color={GlobalStyles.colors.primary400}
                size={size}
              />
            ),
          }}
        />
      </Tab.Navigator>
    );
  }, []);

  return (
    <>
      <StatusBar
        animated={true}
        backgroundColor={GlobalStyles.colors.primary200}
      />
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            headerStyle: {
              backgroundColor: GlobalStyles.colors.primary100,
            },
          }}>
          <Stack.Screen
            name="Todo List"
            component={HomeScreen}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="ToDoList"
            component={ToDoList}
            // open a screen, how modal open
            options={{
              presentation: 'modal',
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
};

export default App;
