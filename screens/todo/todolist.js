import * as React from 'react';
import {useState} from 'react';
import {View, StyleSheet, FlatList, Alert, Image} from 'react-native';
import todoList from '../../constants/constants';
import {GlobalStyles} from '../../constants/style';
import TodoItem from './todoItem';
import ManageTodo from './manageTodo';

const ToDoList = () => {
  const [tasks, setTasks] = useState(todoList);
  const [text, setText] = useState('');
  const [taskId, setTaskId] = useState('');

  const manageTask = () => {
    if (text.length < 5) {
      return;
    }

    if (taskId) {
      const updatedTasks = tasks.map(task => {
        if (task.id === taskId) {
          return {
            ...task,
            text: text,
          };
        }
        return task;
      });

      setTasks(updatedTasks);
      setTaskId('');
      setText('');
    } else {
      const newTask = {
        id: Date.now(),
        text,
        completed: false,
      };
      setTasks([...tasks, newTask]);
      setText('');
    }
  };

  const onChangeTodoText = todoAction => {
    setText(todoAction);
  };

  const deleteTodo = todoId => {
    setTasks(tasks.filter(task => task.id !== todoId));
    setText('');
  };

  const openModal = todoId => {
    Alert.alert('Delete Todo', 'Are you sure, you want to delete todo?', [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {text: 'OK', onPress: () => deleteTodo(todoId)},
    ]);
  };

  const editTodo = todo => {
    setTaskId(todo.id);
    setText(todo.text);
  };

  return (
    <View style={styles.container}>
      <ManageTodo
        manageTask={manageTask}
        onChangeTodoText={onChangeTodoText}
        taskId={taskId}
        text={text}
      />
      {!tasks.length ? (
        <View style={styles.emptyTodoListContainer}>
          <Image
            source={{
              uri: 'https://t4.ftcdn.net/jpg/04/81/04/61/360_F_481046130_OeNYWBmkb8XiAZLIZcmBheUH84tIuUyw.jpg',
            }}
            style={styles.emptyTodoImg}
          />
        </View>
      ) : null}
      <FlatList
        data={tasks}
        keyExtractor={item => item.id}
        renderItem={({item}) => (
          <TodoItem
            key={item.id}
            item={item}
            openModal={openModal}
            editTodo={editTodo}
          />
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: GlobalStyles.colors.primary800,
    flex: 1,
  },
  innerContainer: {
    marginTop: 20,
    paddingHorizontal: 12,
  },
  emptyTodoListContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
  },
  emptyTodoImg: {
    height: 200,
    width: 200,
    borderRadius: 50,
  },
});

export default ToDoList;
