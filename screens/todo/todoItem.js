import React from 'react';
import {GlobalStyles} from '../../constants/style';
import {View, Text, StyleSheet, Button, TouchableOpacity} from 'react-native';

const TodoItem = ({item, editTodo, openModal}) => {
  return (
    <View style={styles.todoListContainer}>
      <View style={styles.innerTodoWrapper}>
        <Text style={styles.todoItemText}>{item.text}</Text>
        <View style={styles.actionItemWrapper}>
          <TouchableOpacity>
            <Button
              title="❌"
              color={GlobalStyles.colors.primary400}
              onPress={() => openModal(item.id)}
            />
          </TouchableOpacity>
          <TouchableOpacity>
            <Button
              title="✏️"
              color={GlobalStyles.colors.primary400}
              onPress={() => editTodo(item)}
            />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  todoListContainer: {
    margin: 10,
  },
  innerTodoWrapper: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  todoItemText: {
    color: GlobalStyles.colors.primary800,
    shadowColor: GlobalStyles.colors.gray500,
    shadowOffset: {width: 1, height: 1},
    shadowOpacity: 0.4,
    backgroundColor: GlobalStyles.colors.primary200,
    padding: 20,
    borderRadius: 10,
    flex: 1,
    marginRight: 20,
  },
  actionItemWrapper: {
    alignItems: 'center',
    flexDirection: 'row',
    gap: 8,
  },
});

export default TodoItem;
