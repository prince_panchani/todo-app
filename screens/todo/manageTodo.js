import React from 'react';
import {GlobalStyles} from '../../constants/style';
import {
  View,
  StyleSheet,
  TextInput,
  Button,
  TouchableOpacity,
} from 'react-native';

const ManageTodo = ({onChangeTodoText, text, taskId, manageTask}) => {
  return (
    <View style={styles.innerContainer}>
      <TextInput
        underlineColorAndroid={'transparent'}
        placeholder={'✍ Enter a Todo Item...'}
        placeholderTextColor={GlobalStyles.colors.primary100}
        onChangeText={onChangeTodoText}
        value={text}
        style={styles.inputText}
      />
      <TouchableOpacity>
        <Button
          title={taskId ? 'Save' : 'Add'}
          color={GlobalStyles.colors.primary400}
          onPress={manageTask}
          disabled={text.length === 0 || text.length < 5}
        />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  innerContainer: {
    marginTop: 20,
    paddingHorizontal: 12,
  },
  inputText: {
    color: GlobalStyles.colors.primary100,
  },
});

export default ManageTodo;
